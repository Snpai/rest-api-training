const express = require('express');
const app = express();
const mongoose = require('mongoose');
const https = require('https');
const fs = require('fs');

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(require('./routes'));


app.get('/',(req,res) => {
     res.send('Hola uwu');
});

mongoose.connect('mongodb://localhost:27017/animales', { useNewUrlParser: true, useCreateIndex: true },
     (err, res) => {
         if (err) throw err;
         console.log('Bd ON!!');
     }
);
try{
     https.createServer({
          key: fs.readFileSync('animalitos.key'),
          cert: fs.readFileSync('animalitos.crt')
     }, app).listen(4001, function(){
          console.log("My https server listening on port 4001");
     });     
}catch(e){
     console.log('Error in https');
}
 
     

app.listen(4000,() => {
     console.log('Escuchando en el puerto 4000');
});

//Todo esto me ahorro al usar JOI
// if(body.species === undefined || body.race === undefined || body.name === undefined || body.age === undefined){
     //      res.send({
     //           ok: false,
     //           err: {
     //                message: "error in the fields"
     //           }
     //      })
     // }else {
     //      if(body.species === "" || body.race === "" || body.name === "" || body.age === ""){
     //           res.send({
     //                ok: false,
     //                err: {
     //                     message: "error in the fields"
     //                }
     //           })               
     //      }else{
     //           if(body.species !== "DOG" && body.species !== "CAT" ){
     //                res.send({
     //                     ok: false,
     //                     err: {
     //                          message: "error in the species"
     //                     }
     //                }) 
     //           }else{
     //                Animal.findOneAndUpdate({_id: id}, body)
     //                .exec((err, animal) => {
     //                     if (err) {
     //                         return res.status(400).json({
     //                             ok: false,
     //                             err
     //                         })
     //                     }
     //                     res.json({
     //                          id,
     //                          ok: true,
     //                          body
                              
     //                     })          
     //                });
     //           }
     //      }
     // }
