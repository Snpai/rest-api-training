const Animal = require('../schemas/animal');
const express = require('express');
const app = express();


app.get("/animal", (req,res) => {
     Animal.find({})
     .exec((err, animales) => {
          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              })
          }
          res.json({
               ok:true,
               animales
          })          
     })

});
app.get("/animal/especie/:tipo",(req,res) => {
     Animal.find({species: req.params.tipo})
     .exec((err, animales) => {
          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              })
          }
          res.json({
               ok:true,
               animales
          })          
     })
});
app.get("/animal/id/:id",(req,res) => {
     Animal.find({_id: req.params.id})
     .exec((err, animales) => {
          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              })
          }
          res.json({
               ok:true,
               animales
          })          
     })
});
app.post('/animal',(req,res) => {
     let body= req.body;
     let animal= new Animal({
          name: body.name,
          age: body.age,
          race: body.race,
          species: body.species
     });

     animal.save((err,animalDB) => {
          if (err) {
               return res.status(400).json({
                   ok: false,
                   err
               })
           }     
           res.json({
                ok: true,
                animal: animalDB
           });     
     });
     
});
app.put('/animal/:id',(req,res,next) => {
     let schema = require('../schemas/animal.schema');
     if (!!schema.validate(req.body).error) {
          return res.status(400).json({
               'error': true,
               'msg': 'Body not valid >:c'
          });
     }
     const id = req.params.id;
     const body = req.body;
     if(isNaN(body.age)){
          res.send({
               ok: false,
               err: {
                    message: "insert a number in the age field"
               }
          })   
     }else{
          body.age= body.age * 1;
     };
     Animal.findOneAndUpdate({_id: id}, body)
     .exec((err, animal) => {
          if (err) {
               return res.status(400).json({
                    ok: false,
                    err
               })
          }
          res.json({
               id,
               ok: true,
               body
               
          })          
     });     
});
app.delete('/animal/id/:id', (req,res) => {
     Animal.findOneAndDelete({_id: req.params.id})
     .exec((err, animales) => {
          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              })
          }
          res.json({
               ok:true,
               msg: `The ID ${req.params.id} has successfully deleted`
          })          
     })
})

module.exports = app;