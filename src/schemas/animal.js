const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let validSpecies = {
     values: ['CAT', 'DOG'],
     message: '{VALUE} dont a valid value'
 }

const AnimalSchema = new Schema({
     name: {
          type: String,
          required: [true,'The name is required']
     },
     age: {
          type: Number,
          required: [true,'The age is required']
     },
     race: {
          type: String,
          required: false
     },
     species:{
          type: String,
          required: [true,'The species attribute is mandatory'],
          enum: validSpecies
     }
})
module.exports = mongoose.model('Animal',AnimalSchema);
