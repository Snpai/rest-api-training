const Joi = require('@hapi/joi');
module.exports = Joi.object({
     name: Joi
          .string()
          .required()
          .min(3),
     age: Joi
          .number()
          .required()
          .min(0)
          .max(500),
     race: Joi
          .string()
          .required(),
     species: Joi
          .string()
          .valid('DOG','CAT')
          .required()
})